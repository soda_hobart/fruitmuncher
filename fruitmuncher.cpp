//Fruit Muncher, a game where a hungry tapir pursues airborne fruit.
//Copyright (C) 2019,  Samuel Russell - soda@sammysvirtualzoo.com

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see https://www.gnu.org/licenses/gpl-3.0.en.html.

#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>
#include <string>
#include <cstdlib>
#include <random>
#include "gameobjects.hpp"
#include <sstream>

int main ()
{

  // sprite handlers
  ObjectLogic<Fruit*> fruit_handler;
  ObjectLogic<Ground*> ground_handler;

  //loading textures
  GameTextures game_textures;
  
  //making ground areas
  ground_factory(100, 500, &ground_handler, &game_textures);
  ground_factory(150, 500, &ground_handler, &game_textures);
  ground_factory(250, 400, &ground_handler, &game_textures);
  ground_factory(300, 400, &ground_handler, &game_textures);
  ground_factory(400, 300, &ground_handler, &game_textures);
  ground_factory(550, 500, &ground_handler, &game_textures);
  ground_factory(600, 500, &ground_handler, &game_textures);
  ground_factory(650, 500, &ground_handler, &game_textures);

  //making background
  sf::Texture bg;
  bg.loadFromFile("game_assets/bg1.png", sf::IntRect(800, 600, 0, 0));


  sf::Sprite background;
  background.setTexture(bg);

  //making fruit
  fruit_factory("raspberry", 200, 200, &fruit_handler, &ground_handler, &game_textures);
  fruit_factory("grapefruit", 100, 200, &fruit_handler, &ground_handler, &game_textures);
  fruit_factory("melon", 400, 50, &fruit_handler, &ground_handler, &game_textures);
  fruit_factory("banana", 700, 300, &fruit_handler, &ground_handler, &game_textures);
  fruit_factory("pineapple", 350, 20, &fruit_handler, &ground_handler, &game_textures);

  for (auto i : fruit_handler.game_objects)
    {
      std::cout << i->fruit_name << ", " << i->fruit_val << endl;
    }

  Tapir tapir = tapir_factory(&fruit_handler, &ground_handler);
  Tapir * dudeman = &tapir;
  sf::Texture tapir_texture;
  tapir_texture.loadFromFile("game_assets/tapir_small1.png", sf::IntRect(0, 0, 100, 61));
  dudeman->setPosition(200, 100);
  dudeman->setTexture(tapir_texture);

  Scoreboard scoreboard = Scoreboard(500, 0);
  Scoreboard message = Scoreboard(0, 0);
  sf::Font font;
  font.loadFromFile("Kumar One_regular.ttf");
  scoreboard.setFont(font);
  scoreboard.setCharacterSize(24);
  message.setFont(font);
  message.setCharacterSize(24);


  //sf::Font font;
  // font.loadFromFile("Kumar One_regular.ttf");
  // sf::Text score_text;
  // score_text.setFont(font);
  // score_text.setCharacterSize(24);

  
  

  //starting the clock for rendering cycle
  sf::Clock clock;

  sf::RenderWindow window(sf::VideoMode(800, 600), "Fruit Muncher");
  //rendering loop
  while (window.isOpen())
    {
      sf::Event event;
      while (window.pollEvent(event))
	{
	  if (event.type == sf::Event::Closed)
	    window.close();
	}

      

      //setting rendering loop to run in 150ms chunks for blocky look
      sf::Time elapsed1 = clock.getElapsedTime();
      sf::Time update = sf::milliseconds(150);

      

      //the following block is a 150ms loop -- game flow control goes here
      if (elapsed1.asMilliseconds() > update.asMilliseconds())
	{
	  
	  //keyboard events
	  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left))
	    {
	      std::cout << "KEYPRESS L" << endl;
	      if (dudeman->heading == "r")
		{
		  std::cout << "HEADING ALTERED" << endl;
		  dudeman->set_heading("l");
		}
	      dudeman->move(-8, 0);
	    }
	  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right))
	    {
	      std::cout << "KEYPRESS R" << endl;
	      if (dudeman->heading == "l")
		{
		  std::cout << "HEADING ALTERED" << endl;
		  dudeman->set_heading("r");
		}
	      dudeman->move(8, 0);
	    }
	  if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up))
	    {
	      if (dudeman->jumped == false and dudeman->grounded == true)
		{
		  dudeman->jumped = true;
		}
	    }
	  //end keyboard events

	  std::cout << dudeman->heading << endl;

	  
	  //score & message events
	  //clearing the message
	  message.setString(message.current_message);
	  message.flash_message(2000);
	  scoreboard.setString(scoreboard.current_score);

	  //applying physics to dudeman
	  dudeman->jump();
	  dudeman->gravity();
	  dudeman->notify_ground_detector();
	  dudeman->notify_fruit_detector();
	  
	  //dudeman->jump();

	  if (dudeman->getGlobalBounds().top > 700)
	    {
	      message.setPosition(200, 200);
	      message.setString("YOU JUST STEPPED IN IT");
	    }

	  //applying physics to fruit
	  for (auto i : fruit_handler.game_objects)
	    {
	      std::cout << i->fruit_name << endl;
	      i->detect_screen_edge(800, 600);
	      i->notify_fruit_detector();
	      i->fruit_move(i->current_speed, i->heading);
	      if (dudeman->getGlobalBounds().intersects(i->getGlobalBounds()))
		{
		  dudeman->in_big_mode = true;
		  if (message.message_flashed == false)
		    {
		      // if (dudeman->heading == "r")
		      // 	{
		      // 	  dudeman->setScale(-2.0, 2.0);
		      // 	}
		      // else if (dudeman->heading == "l")
		      // 	{
		      // 	  dudeman->setScale(2.0, 2.0);
		      // 	}
		      
		      
		      scoreboard.set_score(i->fruit_val);
		    }
		  
		  message.setPosition(i->getGlobalBounds().left, i->getGlobalBounds().top);
		  message.current_message = "WAY TO GO BUDDY!!";
		  message.message_flashed = true;
		}
		// {
		//   stringstream ss;
		//   ss << i->fruit_val;
		//   string fruit_value = ss.str();
		//   string message = fruit_value + " points -- YOU SCORED BUDDY, WAY TO GO!!!!\n";
		//   score_text.setString(message);
		// }
	      //else { score_text.setString(""); }
	    }

	  std::cout << "origin: " << dudeman->getOrigin().x << "," << dudeman->getOrigin().y << endl;
	  std::cout << "gound: " << dudeman->grounded << ", jump: "<< dudeman->jumped << endl;

	  
	  std::cout << "in_big_mode" << dudeman->in_big_mode << endl;
	  //dudeman->big_mode(2000);


	  clock.restart();
	  //end 150ms game cycle
	}

      window.clear(sf::Color::Black);

      window.draw(background);

      window.draw(*dudeman);

      for (auto i : fruit_handler.game_objects)
	{
	  window.draw(*i);
	}

      for (auto i : ground_handler.game_objects)
	{
	  window.draw(*i);
	}

      window.draw(message);
      window.draw(scoreboard);

      window.display();

	}

  return 0;
}
