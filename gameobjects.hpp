//Fruit Muncher, a game where a hungry tapir pursues airborne fruit.
//Copyright (C) 2019,  Samuel Russell - soda@sammysvirtualzoo.com

//This program is free software: you can redistribute it and/or modify
//it under the terms of the GNU General Public License as published by
//the Free Software Foundation, either version 3 of the License, or
//(at your option) any later version.

//This program is distributed in the hope that it will be useful,
//but WITHOUT ANY WARRANTY; without even the implied warranty of
//MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//GNU General Public License for more details.

//You should have received a copy of the GNU General Public License
//along with this program.  If not, see https://www.gnu.org/licenses/gpl-3.0.en.html.

#include <SFML/Graphics.hpp>
#include <iostream>
#include <cmath>
#include <string>
#include <cstdlib>
#include <random>
#include <sstream>

using namespace std;

int random_angle ()
{
  std::random_device random_nums_gen;
  std::mt19937 mt(random_nums_gen());
  std::uniform_int_distribution<> distr(1, 180);
  int random_angle = distr(mt);
  return random_angle;
}

class GameTextures
{
public:
  //fruit textures
  sf::Texture raspberry;
  sf::Texture banana;
  sf::Texture melon;
  sf::Texture pineapple;
  sf::Texture grapefruit;
  //ground textures
  sf::Texture groundslab;
  sf::Texture rocktile;

  GameTextures();
};

GameTextures::GameTextures ()
{
  //fruit textures
  raspberry.loadFromFile("game_assets/raspberry.png", sf::IntRect(0, 0, 80, 78));
  banana.loadFromFile("game_assets/banana.png", sf::IntRect(0, 0, 75, 52));
  melon.loadFromFile("game_assets/melon.png", sf::IntRect(0, 0, 75, 74));
  pineapple.loadFromFile("game_assets/pineapple.png", sf::IntRect(0, 0, 50, 82));
  grapefruit.loadFromFile("game_assets/grapefruit.png", sf::IntRect(0, 0, 75, 48));
  //groundtextures
  groundslab.loadFromFile("game_assets/groundslab.png", sf::IntRect(0, 0, 200, 50));
  rocktile.loadFromFile("game_assets/rocktile.png", sf::IntRect(0, 0, 50, 50));
}

// this class handles collision detection and related memory management
// for the various types of game sprites.
template <class type_GameObject> class ObjectLogic
{
public:
  // the game objects (tapirs, fruit, ground) being observed
  std::vector<type_GameObject> game_objects;
  // objects of a given type registering collisions during this iteration
  // (there are likely to  be three of these...)
  //    -- class calls included in class functions will determine which
  //    -- objects can be bounced off each other, land on each other,
  //    -- get munched, etc.
  std::vector<type_GameObject> collided_objects;

  string message;

  //type_GameObject publish_message ();

  //type_GameObject update (string);

  //polls game objects for collisions
// type_GameObject register_collision (type_GameObject*);
};

//template <class type_GameObject> void ObjectLogic<type_GameObject>::update (string new_message)
// {
//   this->message += new_message;
// }

// template <class type_GameObject> string ObjectLogic<type_GameObject>::publish_message ()
// {
//   return this->message;
// }



// template <class type_GameObject> void ObjectLogic<type_GameObject>::register_collision (type_GameObject game_object)
// {
//   this->collided_objects.push_back(game_object);
// }

// dirt, mulch, rocks, wood, tile, and berber
class Ground: public sf::Sprite
{
public:
  string ground_name;
  ObjectLogic<Ground*> * ground_detector;

  Ground();
  //some stuff
};

Ground::Ground()
{
  ground_name = "regular solid ground";
}

void ground_factory (float x_pos, float y_pos, ObjectLogic<Ground*> * g_detector, GameTextures* textures)
{
  Ground * ground_piece;
  ground_piece = new Ground;
  ground_piece->setPosition(x_pos, y_pos);
  ground_piece->setTexture(textures->rocktile);
  g_detector->game_objects.push_back(ground_piece);
}
  

class Fruit: public sf::Sprite
{
public:
  string fruit_name;
  int fruit_val;
  int heading;
  float current_speed;
  bool bounced;
  //ObjectLogic<Tapir> tapir_detector;
  ObjectLogic<Fruit*> * fruit_detector;
  ObjectLogic<Ground*> * ground_detector; //only pineapple and banana

  void fruit_move (float, float);
  void detect_screen_edge (float, float);
  void bounce();
  void notify_fruit_detector ();
  //void activate(*Observer);
  void notify_bounce();
  Fruit (); //constructor
};

Fruit::Fruit ()
{
  bounced = false;
}

void fruit_factory (string fruit_type,
		    float x_pos,
		    float y_pos,
		    ObjectLogic<Fruit*>* f_detector,
		    ObjectLogic<Ground*>* g_detector,
		    GameTextures* textures)
{
  Fruit * fruit_product;
  fruit_product = new Fruit;
  fruit_product->fruit_name = fruit_type;
  fruit_product->setPosition(x_pos, y_pos);
  if (fruit_type == "raspberry")
    {
      fruit_product->setTexture(textures->raspberry);
      fruit_product->fruit_val = 10;
      fruit_product->current_speed = 40;
      fruit_product->fruit_detector = f_detector;
      fruit_product->ground_detector = g_detector;
      fruit_product->heading = random_angle();
    }
  else if (fruit_type == "banana")
    {
      fruit_product->setTexture(textures->banana);
      fruit_product->fruit_val = 50;
      fruit_product->current_speed = 40;
      fruit_product->fruit_detector = f_detector;
      fruit_product->ground_detector = g_detector;
      fruit_product->heading = random_angle();
    }
  else if (fruit_type == "melon")
    {
      fruit_product->setTexture(textures->melon);
      fruit_product->fruit_val = 100;
      fruit_product->current_speed = 40;
      fruit_product->fruit_detector = f_detector;
      fruit_product->ground_detector = g_detector;
      fruit_product->heading = random_angle();
    }
  else if (fruit_type == "pineapple")
    {
      fruit_product->setTexture(textures->pineapple);
      fruit_product->fruit_val = 200;
      fruit_product->current_speed = 40;
      fruit_product->fruit_detector = f_detector;
      fruit_product->ground_detector = g_detector;
      fruit_product->heading = random_angle();
    }
  else if (fruit_type == "grapefruit")
    {
      fruit_product->setTexture(textures->grapefruit);
      fruit_product->fruit_val = 300;
      fruit_product->current_speed = 40;
      fruit_product->fruit_detector = f_detector;
      fruit_product->ground_detector = g_detector;
      fruit_product->heading = random_angle();
    }
  //adding Fruit reference to ObjectLogic<Fruit> instance
  f_detector->game_objects.push_back(fruit_product);
}




// a hungry tapir
class Tapir: public sf::Sprite
{
public:
  int jump_strength;
  int jump_counter;
  bool jumped;
  string heading;
  int gravity_effects;
  bool grounded;
  int score;
  bool in_big_mode;
  int big_mode_counter;
  sf::FloatRect footing; //for ground collision detection
  ObjectLogic<Fruit*> * fruit_detector;
  ObjectLogic<Ground*> * ground_detector;

  //constructor
  Tapir();

  //jump
  void jump ();

  //gravity
  void gravity ();

  //set heading
  void set_heading(string);

  sf::FloatRect get_footing_area ();

  void check_heading ();

  void notify_fruit_detector ();

  void notify_ground_detector ();

  void big_mode (int);

};


class Scoreboard: public sf::Text
{
public:
  string current_score;
  string current_message;
  //can be score or message to be flashed

  int message_counter;
  //message duration in ms

  bool message_flashed;
  //has a message been flagged and is being currently displayed?
  
  void set_score (int);
  //converts int to string and set score

  void flash_message (int);

  //constructor
  Scoreboard (float, float);
};

Scoreboard::Scoreboard (float pos_x, float pos_y)
{
  current_score = "0";
  //loading font and setting font & size - right now this causes segmentation fault
  //sf::Font font;
  //font.loadFromFile("Kumar One_regular.ttf");
  //this->setFont(font);
  //this->setCharacterSize(24);
  this->setPosition(pos_x, pos_y);

  message_counter = 0;
  current_message = "MUNCH!";
  message_flashed = false;
}

void Scoreboard::flash_message (int duration)
{
  if (this->message_flashed == true)
    {
      this->message_counter += 150;
      if (this->message_counter > duration)
	{
	  this->current_message = "";
	  this->message_counter = 0;
	  this->message_flashed = false;
	}
    }
  else
    {
      this->message_flashed = true;
      this->flash_message(duration);
    }
}

void Scoreboard::set_score (int incr_score)
{
  int i_current_score = std::stoi(this->current_score);
  i_current_score += incr_score;
  stringstream sodastream;
  sodastream << i_current_score;
  string str_incr_score = sodastream.str();
  this->current_score = str_incr_score;
}

Tapir::Tapir ()
{
  jump_strength = -40;
  jump_counter = 0;
  jumped = false;
  grounded = false;
  heading = "l";
}

Tapir tapir_factory (ObjectLogic<Fruit*>* f_detector, ObjectLogic<Ground*>* g_detector)
{
  Tapir tapir;
  tapir.fruit_detector = f_detector;
  tapir.ground_detector = g_detector;
  return tapir;
}

void Tapir::notify_fruit_detector ()
{
  // no good, use iterator
  // make sleepytime now...zzz

  // 1. Loop through collection of game objects (sprites)
  // 2. at each iteration, check to see if the subject (tapir) intersects
  //    with any of the sprites (fruit, ground tiles)
  // 3. If an intersection is detected, a collision is registered with the
  //    ObjectLogic<T> observer instance
  //     * Events that occur when a collision is detected:
  //       + Points are awarded
  //       + Game Over event
  //       + Tapir becomes grounded & sprite position is aligned with Ground sprite
  //       + Some Ground tiles (springs) give the Tapir a super bounce!
  //       + Fruit bounces
  //          - some fruits fly all over the screen
  //          - some fruits bounce off the ground
  //          - some fruits bounce off the ground for a number of times then fall off the screen
  // 4. The subject is responsible for changing its own state?  Yes, in most cases:  for example,
  //    the subject sprite calls its methods like bounce() or setPosition() or change state like
  //    'grounded' or 'munched'.  Let this arrangement be known as "Wipe Your Own Butt," with some
  //    objects being rare exceptions, asking "Wipe My Butt, Please."  For example, Tapir collides
  //    with Fruit, the Fruit instance becomes "munched," the ObjectLogic observer instance belonging
  //    to Tapir instance will direct state changes in the Fruit instance to effect changes like
  //    loading a different texture with "bite marks," or calling Fruit's destructor method.
  //    
  for (auto i : this->fruit_detector->game_objects)
    {
      //std::cout << i->fruit_name << endl;
      if (this->getGlobalBounds().intersects(i->getGlobalBounds()))
	{
	  std::cout << i->fruit_val << " points -- YOU SCORED BUDDY, WAY TO GO!!!!" << endl;
	  //stringstream sstream;
	  //sstream << i->fruit_val;
	  //string fruit_value = sstream.str();
	  //string message = fruit_value + " points -- YOU SCORED BUDDY, WAY TO GO!!!!\n";
	  //fruit_detector->update(message);
	}
		   
    }
}

sf::FloatRect Tapir::get_footing_area ()
{
  float lower = this->getGlobalBounds().top + this->getGlobalBounds().height;
  float upper = this->getGlobalBounds().top;
  float left = this->getGlobalBounds().left;
  float right = this->getGlobalBounds().left + this->getGlobalBounds().width;
  float adj_for_realism = this->getGlobalBounds().width/4;
  float foot_area_top = this->getGlobalBounds().top + (this->getGlobalBounds().height * 0.9);
  float foot_area_height = this->getGlobalBounds().height * 0.9;
  float foot_area_width; //= this->getGlobalBounds().width - adj_for_realism;
  float head_area;
  if (this->heading == "r")
    {
      foot_area_width = this->getGlobalBounds().width - adj_for_realism * 2;
      return sf::FloatRect(left + adj_for_realism, foot_area_top, foot_area_width, foot_area_height);
    }
  else if (this->heading == "l")
    {
      foot_area_width = this->getGlobalBounds().width - adj_for_realism * 1.25;
      return sf::FloatRect(left + adj_for_realism * 2, foot_area_top, foot_area_width, foot_area_height);
    }
  //return sf::FloatRect(left + adj_for_realism, foot_area_top, foot_area_width, foot_area_height);
}

void Tapir::notify_ground_detector ()
{
  bool local_grounded = false;
  for (auto i : this->ground_detector->game_objects)
    {
      if (this->jumped == false)
	{
	  if (this->get_footing_area().intersects(i->getGlobalBounds()))
	    {
	      std::cout << "your on the ground" << endl;
	      this->setPosition(this->getGlobalBounds().left, i->getGlobalBounds().top - this->getGlobalBounds().height);
	      local_grounded = true;
	      std::cout << "grounded(ground_detector_scope)" << this->grounded;
	      std::cout << " -- " << local_grounded << endl;
	    }
	}
    }
  this->grounded = local_grounded;
}



void Tapir::set_heading(string direction)
{
  this->heading = direction;
  if (direction == "r")
    {
      this->setScale(-1.0f, 1.0f);
      this->setOrigin(this->getGlobalBounds().width, 0);
      this->heading = "r";
    }
  else if (direction == "l")
    {
      this->setScale(1.0f, 1.0f);
      this->setOrigin(0, 0);
      this->heading = "l";
    }
}

void Tapir::check_heading ()
{
  if (this->heading == "r")
    {
      this->setScale(-1.0f, 1.0f);
      this->setPosition(this->getGlobalBounds().left + 100, this->getGlobalBounds().top);
    }
}

void Tapir::jump ()
{
  if (this->jumped == true)
    {
      this->grounded = false;
      if (this->jump_counter < 10)
	    {
	      // TODO: change this method to parameterize the inner coefficient so the
	      // "spikiness" of the jump can be assigned parametrically.  This will require
	      // altering the jump_counter, maybe making it parametric as well and using the
	      // y_offset/jump_strength value as the condition for the if statement.
	      float y_offset = this->jump_strength * (cos(10 *(this->jump_counter++ * 3.14159/180)));
	      std::cout << y_offset << "--" << this->jump_counter << endl; 
	      this->move(0, y_offset);
	    }
	  else
	    {
	      this->jump_counter = 0;
	      this->jumped = false;
	    }
    }
}

void Tapir::gravity ()
{
  if (this->grounded == true)
    {
      std::cout << "on the ground" << endl;
      this->gravity_effects = 10;
    }
  else
    {
  if (this->jumped == false)
    {
      if (this->gravity_effects > 18)
	{
	  this->gravity_effects = 18;
	}
      float y_offset = -30 * (cos(10 * (this->gravity_effects++ * 3.14159/180)));
      std::cout << y_offset << "--" <<this->gravity_effects << endl;
      this->move(0, y_offset);
    }
    }
}

// big mode doesn't work, i need to parameterize the scale values used
// by the sf keypress events.
void Tapir::big_mode (int duration)
{
  if (this->in_big_mode == true)
    {
    if (this->big_mode_counter < duration)
    {
      if (this->heading == "r")
	{
	  this->setScale(-2.0f, 2.0f);
	  this->setOrigin(this->getGlobalBounds().width * 2, 0);
	}
      else if (this->heading == "l")
	{
	  this->setScale(2.0f, 2.0f);
	  this->setOrigin(0, 0);
	}
      this->big_mode_counter += 150; //need to make this 150 a const or something
    }
  else
    {
      if (this->heading == "r")
	{
	  this->setScale(-1.0f, 1.0f);
	  this->setOrigin(this->getGlobalBounds().width, 0);
	}
      else if (this->heading == "l")
	{
	  this->setScale(-1.0f, 1.0f);
	  this->setOrigin(0, 0);
	}
      this->in_big_mode = false;
      this->big_mode_counter = 0;
    }
    }
}

void Fruit::fruit_move (float speed, float angle)
{
  if (this->bounced == true)
    {
      float pi = -3.14159;
      float angle_radians = angle*pi/180;
      float dx = speed*cos(angle_radians);
      float dy = speed*sin(angle_radians);
      this->move(dx, 0);
      this->move(0, dy);
    }
  else
    {
      float pi = 3.14159;
      float angle_radians = angle*pi/180;
      float dx = speed*cos(angle_radians);
      float dy = speed*sin(angle_radians);
      this->move(dx, 0);
      this->move(0, dy);
    }
}

void Fruit::notify_fruit_detector ()
{
  for (auto i : this->fruit_detector->game_objects)
    {
      if (this->getGlobalBounds().intersects(i->getGlobalBounds()))
	{
	  if (this->getGlobalBounds() != i->getGlobalBounds()) {this->bounce();}
	}
    }
  
}

void Fruit::bounce ()
{
  //std::random_device random_nums_gen;
  //std::mt19937 mt(random_nums_gen());
  //std::uniform_int_distribution<> distr(1, 180);
  //int random_angle = distr(mt);
  this->heading = random_angle();
  if (this->bounced == true)
    {
      this->bounced = false;
    }
  else
    {
      this->bounced = true;
    }
}

void Fruit::detect_screen_edge (float screen_x, float screen_y)
{
  if (this->getGlobalBounds().top < 0)
    {
      this->setPosition(this->getGlobalBounds().left, 1);
      this->bounce();
    }
  else if (this->getGlobalBounds().left < 0)
    {
      this->setPosition(1, this->getGlobalBounds().top);
      this->bounce();
    }
  else if (this->getGlobalBounds().top + this->getGlobalBounds().height > screen_y)
    {
      this->setPosition(this->getGlobalBounds().left, screen_y - this->getGlobalBounds().height - 1);
      this->bounce();
    }
  else if (this->getGlobalBounds().left + this->getGlobalBounds().width > screen_x)
    {
      this->setPosition(screen_x - this->getGlobalBounds().width -1, this->getGlobalBounds().top);
      this->bounce();
    }
}


