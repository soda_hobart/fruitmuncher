# Fruit Muncher

Game where a hungry tapir attempts to eat unpredictable fruit

## About Fruit Muncher

Fruit Muncher is a simple platform jumping game that I made as an exercise in learning a bit of C++.  See road map for project plans.  Please contact <soda@sammysvirtualzoo.com> with any suggestions or feedback.

## How to run Fruit Muncher

These instructions are for GNU/Linux operating system (for MacOS & Windows see <https://www.sfml-dev.org/tutorials/2.5/>).

1. Simple and Fast Multimedia Library development files are required
    -   Debian/Ubuntu/Mint/etc: ```sudo apt-get install libsfml-dev```
2. Download or clone repository
    -   ```git clone "<https://bitbucket.org/soda_hobart/fruitmuncher.git>"```
3. Compile Fruit Muncher
    -   ```g++ -c -std=c++11 fruitmuncher.cpp```
    -   ```g++ -o FruitMuncher -c -std=c++11 fruitmuncher.o -lsfml-graphics -lsfml-system -lsfml-window```
4. Run Fruit Muncher with ```./FruitMuncher```

## How to play Fruit Muncher

-   Catch the fruit, watch your score go up (doesn't work that well presently).
-   Don't fall, if you do, just close the game and restart the application.

## Fruit Muncher road map

At the time of this writing (March 2019), Fruit Muncher has all of functions that I set out to create:  basic rendering, keyboard controls, basic 2D physics and collision detection.  The next step in the project is to overhaul all parts of the project, including the codebase and art & graphics, with the goal of creating a game that's actually fun.
-   Refine and expand use of design patterns to establish a more modular application structure that allows for a wider variety of states and behaviors for game entities.
-   Experiment with pace and style of gameplay.
-   Work on the graphics image assets to achieve more visual unity while maintaining the "ClipArt" aesthetic.  Create animated sequences for things like movement, munching, and other astonishing events.
-   Include sound effects and music.

## License

Fruit Muncher is free software licensed under GNU General Public License version 3.  See the license.txt file included in the repository for more information.
